/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <SDL.h>
#include "r2sdl.h"

static SDL_Surface *surface = NULL;

SDL_Surface *r2sdl_begin(int w, int h) {
    SDL_Init(SDL_INIT_VIDEO);
    surface = SDL_SetVideoMode(w, h, 8, SDL_SWSURFACE);
    return (surface);
}

void r2sdl_end(void) {
    SDL_Quit();
}

void r2sdl_setpalette(rgb256 *p) {
    int c;
    SDL_Color colors[256];
    for (c = 0; c < 256; c++) {
        colors[c].r = (*p)[c][0];
        colors[c].g = (*p)[c][1];
        colors[c].b = (*p)[c][2];
    }
    SDL_SetColors(surface, colors, 0, 256);
}

void r2sdl_display(raster *rb) {
    int x, y;
    Uint8 *ps = (Uint8 *) surface->pixels;
    Uint8 *psx;
    Uint8 *pr = rb->data;
    SDL_LockSurface(surface);
    for (y = 0; y < rb->h; y++) {
        psx = ps;
        for (x = 0; x < rb->w; x++) {
            *psx++ = *pr++;
        }
        ps += surface->pitch;
    }
    SDL_UnlockSurface(surface);
}

/* EOF */

