/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* coordinate geometry */

#include "geometry.h"
#include "fixed.h"

void v2_add(vector2 *p, vector2 *q, vector2 *to) {
    (*to)[0] = f_add((*p)[0], (*q)[0]);
    (*to)[1] = f_add((*p)[1], (*q)[1]);
}

void v2_sub(vector2 *p, vector2 *q, vector2 *to) {
    (*to)[0] = f_sub((*p)[0], (*q)[0]);
    (*to)[1] = f_sub((*p)[1], (*q)[1]);
}

int v2_rturn(vector2 *p, vector2 *q, vector2 *r) {
    return (
        (((int)((*q)[0]))*((int)((*r)[1])) + 
         ((int)((*p)[0]))*((int)((*q)[1])) +
         ((int)((*r)[0]))*((int)((*p)[1]))) -
        (((int)((*q)[0]))*((int)((*p)[1])) +
         ((int)((*r)[0]))*((int)((*q)[1])) + 
         ((int)((*p)[0]))*((int)((*r)[1]))) < 0
    );
}

void v3_add(vector3 *p, vector3 *q, vector3 *to) {
    (*to)[0] = f_add((*p)[0], (*q)[0]);
    (*to)[1] = f_add((*p)[1], (*q)[1]);
    (*to)[2] = f_add((*p)[2], (*q)[2]);
}

void v3_sub(vector3 *p, vector3 *q, vector3 *to) {
    (*to)[0] = f_sub((*p)[0], (*q)[0]);
    (*to)[1] = f_sub((*p)[1], (*q)[1]);
    (*to)[2] = f_sub((*p)[2], (*q)[2]);
}

void v3_project(vector3 *p, fixed d, vector2 *to) {
    (*to)[0] = f_div(f_mul((*p)[0], d), f_sub(d, (*p)[2]));
    (*to)[1] = f_div(f_mul((*p)[1], d), f_sub(d, (*p)[2]));
}

void v4_add(vector4 *p, vector4 *q, vector4 *to) {
    (*to)[0] = f_add((*p)[0], (*q)[0]);
    (*to)[1] = f_add((*p)[1], (*q)[1]);
    (*to)[2] = f_add((*p)[2], (*q)[2]);
    (*to)[3] = f_add((*p)[3], (*q)[3]);
}

void v4_sub(vector4 *p, vector4 *q, vector4 *to) {
    (*to)[0] = f_sub((*p)[0], (*q)[0]);
    (*to)[1] = f_sub((*p)[1], (*q)[1]);
    (*to)[2] = f_sub((*p)[2], (*q)[2]);
    (*to)[3] = f_sub((*p)[3], (*q)[3]);
}

void v4_project(vector4 *p, fixed d, vector3 *to) {
    (*to)[0] = f_div(f_mul((*p)[0], d), f_sub(d, (*p)[3]));
    (*to)[1] = f_div(f_mul((*p)[1], d), f_sub(d, (*p)[3]));
    (*to)[2] = f_div(f_mul((*p)[2], d), f_sub(d, (*p)[3]));
}

void m44_mul(matrix44 *p, matrix44 *q, matrix44 *to) {
    int i, j, k;
    int s;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            s = 0;
            for (k = 0; k < 4; k++) {
                s += ((*p)[i][k] * (*q)[k][j]);
            }
            (*to)[i][j] = s >> FIXED_BITS;
        }
    }
}

void v4m44_mul(vector4 *v, matrix44 *m, vector4 *to) {
    int i, j;
    int s;
    for (i = 0; i < 4; i++) {
        s = 0;
	for (j = 0; j < 4; j++) {
            s += ((*v)[j] * (*m)[i][j]);
        }
        (*to)[i] = s >> FIXED_BITS;
    }
}

void m44_id(matrix44 *m) {
    int i, j;
    for (i = 0; i < 4; i++) {
        for (j = 0; j < 4; j++) {
            (*m)[i][j] = f_itof(i == j);
        }
    }
}

void m44_rot1(matrix44 *m, int d1, int d2, int a) {
    fixed c, s;
    m44_id(m);
    c = f_cos(a);
    s = f_sin(a);
    (*m)[d1][d1] = c; (*m)[d2][d1] = -s;
    (*m)[d1][d2] = s; (*m)[d2][d2] =  c;
}

void m44_rot6(matrix44 *m, int a01, int a02, int a03,
                           int a12, int a13, int a23) {
    matrix44 m01, m02, m03, m12, m13, m23;
    matrix44 m0102, m0312, m1323;
    matrix44 m01020312;
    m44_rot1(&m01, 0, 1, a01);
    m44_rot1(&m02, 0, 2, a02);
    m44_rot1(&m03, 0, 3, a03);
    m44_rot1(&m12, 1, 2, a12);
    m44_rot1(&m13, 1, 3, a13);
    m44_rot1(&m23, 2, 3, a23);
    m44_mul(&m01, &m02, &m0102);
    m44_mul(&m03, &m12, &m0312);
    m44_mul(&m13, &m23, &m1323);
    m44_mul(&m0102, &m0312, &m01020312);
    m44_mul(&m01020312, &m1323, m);
}

/* EOF */
