/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* stack */

#include "stack.h"

stack_content s_at(stack *s, int n) {
    if (n < 0) {
        n = s->head + 1 + n;
    }
    return (s->body[n]);
}

void s_delete(stack *s, int n) {
    int i;
    if (n < 0) {
        n = s->head + 1 + n;
    }
    for (i = n + 1; i < s->head + 1; i++) {
        s->body[i-1] = s->body[i];
    }
    (s->head)--;
}

void s_copy(stack *s, stack *d) {
    int i;
    d->head = s->head;
    for (i = 0; i < s->head + 1; i++) {
        d->body[i] = s->body[i];
    }
}

void s_sort(stack *s, comparator c) {
    stack_content v;
    int i;
    int j;
    for (i = 1; i <= s->head; i++) {
        v = s->body[i];
        for (j = i-1; j >= 0 && c(s->body[j], v) > 0; j--) {
            s->body[j+1] = s->body[j];
        }
        s->body[j+1] = v;
    }
}

void s_uniq(stack *s, comparator c) {
    int r, w;
    w = 0;
    for (r = 1; r < s->head + 1; r++) {
        if (s->body[w] != s->body[r]) {
            w++;
            s->body[w] = s->body[r];
        }
    }
    s->head = w;
}

void s_reverse(stack *s) {
    int i,j;
    stack_content v;
    for (i = 0, j = s->head; i < j; i++, j--) {
        v = s->body[i];
        s->body[i] = s->body[j];
        s->body[j] = v;
    }
}

void s_append(stack *s, stack *d) {
    int i;
    for (i = 0; i < s->head + 1; i++) {
        d->body[d->head + 1 + i] = s->body[i];
    }
    d->head += s->head + 1;
}

/* EOF */
