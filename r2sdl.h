/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef R2SDL_H
#define R2SDL_H 1
/* interface to SDL display */

#include <SDL.h>

#include "palette.h"
#include "raster.h"

SDL_Surface *r2sdl_begin(int w, int h);
void r2sdl_end(void);
void r2sdl_setpalette(rgb256 *p);
void r2sdl_display(raster *rb);

/* EOF */
#endif

