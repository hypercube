/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006, 2007 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* render a hypercube in different ways */

#include <limits.h>

#include "cube4d.h"
#include "fixed.h"
#include "geometry.h"
#include "hull2d.h"
#include "raster.h"
#include "render.h"
#include "stack.h"

/* project 4D to 2D */
void n_project(c4dv *from, fixed d43, fixed d32, c4d2dv *to) {
    int i;
    vector3 v3;
    for (i = 0; i < 16; i++) {
        v4_project(&((*from)[i]), d43, &v3);
        v3_project(&v3, d32, &((*to)[i]));
    }
}

/* scale 2D to fit screen */
void n_scale2screen(c4d2dv *v2, int w, int h) {
    int i;
    static int min[2] = { INT_MAX, INT_MAX };
    static int max[2] = { INT_MIN, INT_MIN };
    for (i = 0; i < 16; i++) {
        min[0] = (*v2)[i][0] < min[0] ? (*v2)[i][0] : min[0];
        min[1] = (*v2)[i][1] < min[1] ? (*v2)[i][1] : min[1];
        max[0] = (*v2)[i][0] > max[0] ? (*v2)[i][0] : max[0];
        max[1] = (*v2)[i][1] > max[1] ? (*v2)[i][1] : max[1];
    }
    for (i = 0; i < 16; i++) {
        (*v2)[i][0] -= (max[1] + min[0]) >> 1;
        (*v2)[i][1] -= (max[1] + min[1]) >> 1;
        (*v2)[i][0] *= (w>>4);
        (*v2)[i][1] *= (h>>4);
        (*v2)[i][0] /= ((max[0] - min[0])>>4)+16;
        (*v2)[i][1] /= ((max[1] - min[1])>>4)+16;
        (*v2)[i][0] += (w>>1);
        (*v2)[i][1] += (h>>1);
    }
}

/* render edges */
void n_wireframe(raster *rb, c4d2dv *v2, int pen) {
    int i;
    for (i = 0; i < 32; i = i + 1) {
        r_line(
            rb,
            (*v2)[cube4d_trail[i  ]][0],
            (*v2)[cube4d_trail[i  ]][1],
            (*v2)[cube4d_trail[i+1]][0],
            (*v2)[cube4d_trail[i+1]][1],
            pen
        );
    }
}

/* render sub-cubes as layered polygons */
void n_layered(raster *rb, raster *tb, c4d2dv *v2) {
    int i, j;
    stack s, t;
    fixed bounds[4];
    for (i = 0; i < 8; i++) {
        r_clear(tb, 0);
        s_clear(&s);
        for (j = 0; j < 8; j++) {
            s_push(&s, &((*v2)[cube4d_cube[i][j]]));
        }
        hull2d(&s, &t);
        r_fillpoly(tb, &t, 1 << i);
	r_bounds(rb, &t, bounds, 1);
        r_bitor_bounded(rb, tb, bounds);
    }
}

/* EOF */
