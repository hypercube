/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* cube4d object */

#include "cube4d.h"
#include "fixed.h"
#include "geometry.h"
#include "stack.h"

/* vertices */
int cube4d_vertices = 16;
vector4 cube4d_vertex[16];

/* Euler trail */
int cube4d_edges = 32;
int cube4d_trailsize = 33;
int cube4d_trail[33] = {
    0,  1,  3,  2,  6, 14, 10,  8,
    9, 11,  3,  7, 15, 14, 12, 13,
    9,  1,  5,  7,  6,  4, 12,  8,
    0,  4,  5, 13, 15, 11, 10,  2,
    0
};

/* sub-cubes */
int cube4d_cubes = 8;
int cube4d_cube[8][8];

/* comparator */
static comparator cmp_int;
static int cmp_int(stack_content a, stack_content b) {
    int ai, bi;
    ai = (int) a;
    bi = (int) b;
    return (ai - bi);
}

/* initialise */
void cube4d_init(void) {
    int i, j, v, b, h;
    stack s;
    for (i = 0; i < 16; i++) {
        for (j = 0; j < 4; j++) {
            cube4d_vertex[i][j] = (i & 1<<j) ? FIXED_ONE : f_neg(FIXED_ONE);
        }
    }
    for (i = 0; i < 8; i++) {
        s_clear(&s);
        for (v = 0; v < 16; v++) {
            b = i >> 1;
            h = i & 1;
            if (h) {
                s_push(&s, (stack_content) (v | (1<<b)));
            } else {
                s_push(&s, (stack_content) (v &~(1<<b)));
            }
        }
        s_sort(&s, cmp_int);
        s_uniq(&s, cmp_int);
        for (j = 0; j < 8; j++) {
            cube4d_cube[i][j] = (int) s_at(&s, j);
        }
    }
}

/* EOF */
