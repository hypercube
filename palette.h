/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef PALETTE_H
#define PALETTE_H 1
/* funky colour palettes */

#include "fixed.h"

typedef fixed rgb[3];
typedef rgb rgb256[256];

#define rgb_fto64(x) (((x) >> 1) & 63)

void rgb_zero(rgb *to);
void rgb_copy(rgb *to, rgb *from);
void rgb_add(rgb *to, rgb *from);
void rgb_mul(rgb *to, rgb *from);
void rgb_smul(rgb *to, fixed f);
void rgb_sdiv(rgb *to, fixed f);
void rgb_normalize(rgb *to);

void p_planar8bit(rgb256 *p, int a);
void p_set(rgb256 *p);

/* EOF */
#endif
