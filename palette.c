/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* funky colour palettes */

#include "fixed.h"
#include "palette.h"

void rgb_zero(rgb *to) {
    (*to)[0] = f_itof(0);
    (*to)[1] = f_itof(0);
    (*to)[2] = f_itof(0);
}

void rgb_one(rgb *to) {
    (*to)[0] = f_itof(1);
    (*to)[1] = f_itof(1);
    (*to)[2] = f_itof(1);
}

void rgb_copy(rgb *to, rgb *from) {
    (*to)[0] = (*from)[0];
    (*to)[1] = (*from)[1];
    (*to)[2] = (*from)[2];
}

void rgb_add(rgb *to, rgb *from) {
    (*to)[0] += (*from)[0];
    (*to)[1] += (*from)[1];
    (*to)[2] += (*from)[2];
}

void rgb_mul(rgb *to, rgb *from) {
    (*to)[0] = f_mul((*to)[0], (*from)[0]);
    (*to)[1] = f_mul((*to)[1], (*from)[1]);
    (*to)[2] = f_mul((*to)[2], (*from)[2]);
}

void rgb_sadd(rgb *to, fixed f) {
    (*to)[0] = f_add((*to)[0], f);
    (*to)[1] = f_add((*to)[1], f);
    (*to)[2] = f_add((*to)[2], f);
}

void rgb_smul(rgb *to, fixed f) {
    (*to)[0] = f_mul((*to)[0], f);
    (*to)[1] = f_mul((*to)[1], f);
    (*to)[2] = f_mul((*to)[2], f);
}

void rgb_sdiv(rgb *to, fixed f) {
    (*to)[0] = f_div((*to)[0], f);
    (*to)[1] = f_div((*to)[1], f);
    (*to)[2] = f_div((*to)[2], f);
}

void rgb_normalize(rgb *to) {
    fixed max;
    int i;
    max = 0;
    for (i = 0; i < 3; i++) {
        if ((*to)[i] > max) {
            max = (*to)[i];
        }
    }
    if (max) {
        rgb_sdiv(to, max + 1);
    }
}

void p_planar8bit(rgb256 *p, int a) {
    int c, b, n;
    rgb core[8];
    rgb colour;
    for (c = 0; c < 8; c++) {
        core[c][0] = (128 + f_sin((c << 6) + a +   0));
        core[c][1] = (128 + f_sin((c << 6) + a + 171));
        core[c][2] = (128 + f_sin((c << 6) + a + 341));
    }
    for (c = 0; c < 256; c++) {
        rgb_zero(&colour);
        n = 0;
        for (b = 0; b < 8; b++) {
            if (c & (1<<b)) {
                rgb_add(&colour, &(core[b]));
                n++;
            }
        }
	if (n) {
	    rgb_sdiv(&colour, f_itof(n));
	}
        rgb_copy(&((*p)[c]), &colour);
    }
}

/* EOF */
