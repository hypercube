/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef GEOMETRY_H
#define GEOMETRY_H 1
/* coordinate geometry */

#include "fixed.h"

typedef fixed vector2[2];
typedef fixed vector3[3];
typedef fixed vector4[4];
typedef fixed matrix44[4][4];

void v2_add(vector2 *p, vector2 *q, vector2 *to);
void v2_sub(vector2 *p, vector2 *q, vector2 *to);
int v2_rturn(vector2 *p, vector2 *q, vector2 *r);

void v3_add(vector3 *p, vector3 *q, vector3 *to);
void v3_sub(vector3 *p, vector3 *q, vector3 *to);
void v3_project(vector3 *p, fixed d, vector2 *to);

void v4_add(vector4 *p, vector4 *q, vector4 *to);
void v4_sub(vector4 *p, vector4 *q, vector4 *to);
void v4_project(vector4 *p, fixed d, vector3 *to);

void m44_mul(matrix44 *p, matrix44 *q, matrix44 *to);
void v4m44_mul(vector4 *v, matrix44 *m, vector4 *to);
void m44_id(matrix44 *m);
void m44_rot1(matrix44 *m, int d1, int d2, int a);
void m44_rot6(matrix44 *m, int a01, int a02, int a03,
                           int a12, int a13, int a23);

/* EOF */
#endif
