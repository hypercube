/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* fixed point arithmetic */

#include <math.h>

#include "fixed.h"

void f_init(void) {
    int i;
    /* fill sinetable */
    for (i = 0; i < FIXED_TABSIZE; i++) {
        f_sintab[i] = FIXED_ONE * sin((double) i * 2 * M_PI / FIXED_TABSIZE);
    }
}

int f_cmp(fixed a, fixed b) {
    return (a - b);
}

/* EOF */

