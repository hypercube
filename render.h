/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef RENDER_H
#define RENDER_H 1
/* render a hypercube in different ways */

#include "cube4d.h"
#include "fixed.h"
#include "geometry.h"
#include "raster.h"

typedef vector2 c4d2dv[16];

/* project 4d to 2d */
void n_project(c4dv *from, fixed d43, fixed d32, c4d2dv *to);

/* scale 2d to fit screen */
void n_scale2screen(c4d2dv *v2, int w, int h);

/* render edges */
void n_wireframe(raster *rb, c4d2dv *v2, int pen);

/* render sub-cubes as layered polygons */
void n_layered(raster *rb, raster *tb, c4d2dv *v2);

/* EOF */
#endif
