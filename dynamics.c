/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* movement */

#include "cube4d.h"
#include "dynamics.h"
#include "fixed.h"
#include "geometry.h"

static matrix44 m_rot;

void d_init(void) {
    m44_id(&m_rot);
}

void d_transform(c4dv *from, c4dv *to,
    int a01, int a02, int a03, int a12, int a13, int a23
) {
    static int r01 = 0;
    static int r02 = 0;
    static int r03 = 0;
    static int r12 = 0;
    static int r13 = 0;
    static int r23 = 0;
    int i;
    r01 += a01;
    r02 += a02;
    r03 += a03;
    r12 += a12;
    r13 += a13;
    r23 += a23;
    m44_rot6(&m_rot,
        r01,
        r02,
        r03,
        r12,
        r13,
        r23
    );
    for (i = 0; i < 16; i++) {
        v4m44_mul(&((*from)[i]), &m_rot, &((*to)[i]));
    }
}

/* EOF */
