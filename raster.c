/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006, 2007 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* raster drawing algorithms */

#include <stdlib.h>
#include <string.h>

#include "geometry.h"
#include "raster.h"
#include "stack.h"

raster *r_alloc(int w, int h) {
    raster *rb;
    rb = (raster *) malloc(sizeof(raster) + w*h);
    if (rb) {
        rb->w = w;
        rb->h = h;
        rb->data = (pixel *) (((char *) rb) + sizeof(raster));
    }
    return (rb);
}

void r_free(raster *rb) {
    if (rb) {
        free (rb);
    }
}

void r_clear(raster *rb, pixel pen) {
    int n = rb->w * rb-> h;
    memset(rb->data, (int) pen, n);
}

void r_plot(raster *rb, int x, int y, pixel pen) {
    rb->data[y*rb->w+x] = pen;
}

pixel r_get(raster *rb, int x, int y) {
    return (rb->data[y*rb->w+x]);
}

void r_hline(raster *rb, int x0, int y, int x1, pixel pen) {
    int x;
    pixel *p = &(rb->data[y*rb->w+x0]);
    for (x = x0; x <= x1; x++) {
        *p++ = pen;
    }
}

void r_line(raster *rb, int x0, int y0, int x1, int y1, pixel pen) {
    short steep, t, dx, dy, e, de, x, y, ystep;
    steep = abs(y1 - y0) > abs(x1 - x0);
    if (steep) {
        t = x0; x0 = y0; y0 = t;
        t = x1; x1 = y1; y1 = t;
    }
    if (x0 > x1) {
        t = x0; x0 = x1; x1 = t;
        t = y0; y0 = y1; y1 = t;
    }
    dx = x1 - x0;
    dy = abs(y1 - y0);
    e = 0;
    de = dy;
    y = y0;
    ystep = (y0 < y1) ? 1 : -1;
    for (x = x0; x <= x1; x++) {
        if (steep) {
            r_plot(rb, y, x, pen);
        } else {
            r_plot(rb, x, y, pen);
        }
        e += de;
        if ((e<<1) >= dx) {
            y += ystep;
            e -= dx;
        }
    }
}

void r_poly(raster *rb, stack *p, pixel pen) {
    int i;
    vector2 *p0, *p1;
    for (i = 0; i < s_size(p); i++) {
        p0 = (vector2 *) s_at(p, i - 1);
        p1 = (vector2 *) s_at(p, i);
        r_line(rb, (*p0)[0], (*p0)[1], (*p1)[0], (*p1)[1], pen);
    }
}

#define MAX(a, b) ((a) > (b) ? (a) : (b))
#define MIN(a, b) ((a) < (b) ? (a) : (b))

/* bounds should be a fixed[4] */
void r_bounds(raster *rb, stack *p, fixed *bounds, int clear_bounds) {
    int i;
    vector2 *tmp;
    if (clear_bounds)
    {
    	bounds[0] = rb->w - 1;
    	bounds[1] = rb->h - 1;
    	bounds[2] = 0;
    	bounds[3] = 0;
    }
    for (i = 0; i < s_size(p); i++) {
        tmp = (vector2 *) s_at(p, i);
        bounds[0] = MIN(bounds[0], (*tmp)[0]);
        bounds[1] = MIN(bounds[1], (*tmp)[1]);
        bounds[2] = MAX(bounds[2], (*tmp)[0]);
        bounds[3] = MAX(bounds[3], (*tmp)[1]);
    }
    /* ensure bounds are within rb limits */
    bounds[0] = MAX(bounds[0], 0);
    bounds[1] = MAX(bounds[1], 0);
    bounds[2] = MIN(bounds[2], rb->w - 1);
    bounds[3] = MIN(bounds[3], rb->h - 1);
}

void r_fillpoly(raster *rb, stack *p, pixel pen) {
    int x, y, x0, x1;
    fixed bounds[4];
    r_clear(rb, 0);
    r_poly(rb, p, 1);
    r_bounds(rb, p, bounds, 1);
    for (y = bounds[1]; y <= bounds[3]; y++) {
        x0 = rb->w+1;
        x1 = -2;
        for (x = bounds[0]; x <= bounds[2]; x++) {
            if (r_get(rb, x, y)) {
                x0 = x;
                break;
            }
        }
        for (x = bounds[2]; x >= bounds[0]; x--) {
            if (r_get(rb, x, y)) {
                x1 = x;
                break;
            }
        }
        if (x1 >= x0) {
            r_hline(rb, x0, y, x1, pen);
        }
    }
}

void r_copy(raster *to, raster *from) {
    int n = to->w * to->h;
    memcpy(to->data, from->data, n);
}

void r_bitor(raster *to, raster *from) {
    int i;
    int n = to->w * to->h;
    for (i = 0; i < n; i++) {
        to->data[i] |= from->data[i];
    }
}

void r_bitor_bounded(raster *to, const raster *from, const fixed *bounds) {
    int xstart;
    int last;
    xstart = bounds[0] + bounds[1] * to->w;
    last = bounds[0] + bounds[3] * to->w;
    for (; xstart <= last; xstart += to->w) {
    	pixel *tx = to->data + xstart;
    	pixel *fx = from->data + xstart;
    	pixel *lastx = tx + (bounds[2] - bounds[0]);
    	for (; tx <= lastx;) {
            *tx++ |= *fx++;
        }
    }
}

void r_bitand(raster *to, raster *from) {
    int i;
    int n = to->w * to->h;
    for (i = 0; i < n; i++) {
        to->data[i] &= from->data[i];
    }
}

void r_bitxor(raster *to, raster *from) {
    int i;
    int n = to->w * to->h;
    for (i = 0; i < n; i++) {
        to->data[i] ^= from->data[i];
    }
}

/* EOF */
