/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef CUBE4D_H
#define CUBE4D_H 1
/* cube4d object */

#include "geometry.h"

/* vertices */
typedef vector4 c4dv[16];
extern c4dv cube4d_vertex;
extern int cube4d_vertices;

/* Euler trail */
extern int cube4d_edges;
extern int cube4d_trail[33];
extern int cube4d_trailsize;

/* sub-cubes */
extern int cube4d_cubes;
extern int cube4d_cube[8][8];

/* initialise vertices */
void cube4d_init(void);

/* EOF */
#endif

