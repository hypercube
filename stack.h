/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006,2017 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef STACK_H
#define STACK_H 1
/* stack */

#include <stdint.h>

#define S_MAXSIZE (64)

typedef intptr_t stack_content;

typedef int (comparator)(stack_content, stack_content);

typedef struct {
    short head;
    stack_content body[S_MAXSIZE];
} stack;

#define s_clear(s) ((s)->head = -1)
#define s_size(s) ((s)->head + 1)
#define s_push(s,x) ((s)->body[++((s)->head)] = (stack_content)(x))
#define s_pop(s) ((s)->body[((s)->head)--])
#define s_squash(s) (((s)->body[(s)->head-1] = \
    (s)->body[(s->head)]),((s)->head--))
stack_content s_at(stack *s, int n);
void s_delete(stack *s, int n);
void s_copy(stack *s, stack *d);
void s_sort(stack *s, comparator c);
void s_uniq(stack *s, comparator c);
void s_reverse(stack *s);
void s_append(stack *s, stack *d);

/* EOF */
#endif

