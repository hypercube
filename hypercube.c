/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006, 2007 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* test rendering */

#include <stdlib.h>
#include <sys/poll.h>

#ifdef HAVE_LIBLO
#include <lo/lo.h>
#endif

#include <SDL.h>

#include "cube4d.h"
#include "dynamics.h"
#include "fixed.h"
#include "geometry.h"
#include "palette.h"
#include "r2sdl.h"
#include "raster.h"
#include "render.h"
#include "sdl2png.h"
#include "stack.h"

int d1 = 512;
int d2 = 1024;
int a[4][4] = {
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 },
    { 0, 0, 0, 0 }
};

int running = 1;

#ifdef HAVE_LIBLO
void osc_error_handler(int n, const char *msg, const char *path) {
    printf("liblo server error %d in path %s: %s\n", n, path, msg);
}

int osc_quit_handler(
    const char *path, const char *types,
    lo_arg **argv, int argc, void *data, void *user_data
) {
    running = 0;
    return (0);
}

int osc_rotate_inc_handler(
    const char *path, const char *types,
    lo_arg **argv, int argc, void *data, void *user_data
) {
    int dim1 = argv[0]->i;
    int dim2 = argv[1]->i;
    if (
        0 <= dim1 && dim1 < 4 &&
        0 <= dim2 && dim2 < 4 &&
	dim1 < dim2
    ) {
        a[dim1][dim2]++;
    }
    return (0);
}

int osc_rotate_dec_handler(
    const char *path, const char *types,
    lo_arg **argv, int argc, void *data, void *user_data
) {
    int dim1 = argv[0]->i;
    int dim2 = argv[1]->i;
    if (
        0 <= dim1 && dim1 < 4 &&
        0 <= dim2 && dim2 < 4 &&
	dim1 < dim2
    ) {
        a[dim1][dim2]--;
    }
    return (0);
}

int osc_rotate_zero_handler(
    const char *path, const char *types,
    lo_arg **argv, int argc, void *data, void *user_data
) {
    int dim1 = argv[0]->i;
    int dim2 = argv[1]->i;
    if (
        0 <= dim1 && dim1 < 4 &&
        0 <= dim2 && dim2 < 4 &&
	dim1 < dim2
    ) {
        a[dim1][dim2]=0;
    }
    return (0);
}

void osc_send_v2(lo_address sender, c4d2dv *v2) {
    int i;
    for (i = 0; i < 16; i++) {
        lo_send(sender, "/hypercube/vertex2d", "iff",
	    i, (float) (*v2)[i][0], (float) (*v2)[i][1]
	);
    }
}
#endif

int main(int argc, char **argv) {

    int w, h, t;
    raster *rb = NULL;
    raster *tb = NULL;
    rgb256 p;
    c4dv v4;
    c4d2dv v2;
    int paused = 0;
    int save = 0;
    SDL_Event event;
    SDL_Surface *surface = NULL;

#ifdef HAVE_LIBLO
    int oscfd;
    char *oscrport;
    lo_server oscserver;
    struct pollfd pollfds[1];

    char *oscsport;
    lo_address sender;
#endif

    char filename[64];
    char *fileformat = "hypercube-%08d.png";
    int filecount = 0;
    int savecount = 512;

    if (argc != 8) exit(1);

    w = atoi(argv[1]);
    h = atoi(argv[2]);
    d1 = atoi(argv[3]);
    d2 = atoi(argv[4]);
    savecount = atoi(argv[5]);

#ifdef HAVE_LIBLO
    oscrport = argv[6];
    oscsport = argv[7];
#endif

    w = w < 64 ? 64 : w;
    h = h < 64 ? 64 : h;
    d1 = d1 < 512 ? 512 : d1;
    d2 = d2 < 512 ? 512 : d2;
    savecount = savecount < 1 ? 1 : savecount;

#ifdef HAVE_LIBLO
    oscserver = lo_server_new(oscrport, osc_error_handler);
    lo_server_add_method(oscserver, "/hypercube/rotate/inc",  "ii", osc_rotate_inc_handler, NULL);
    lo_server_add_method(oscserver, "/hypercube/rotate/dec",  "ii", osc_rotate_dec_handler, NULL);
    lo_server_add_method(oscserver, "/hypercube/rotate/zero", "ii", osc_rotate_zero_handler, NULL);
    lo_server_add_method(oscserver, "/hypercube/quit", "", osc_quit_handler, NULL);
    oscfd = lo_server_get_socket_fd(oscserver);
    sender = lo_address_new(NULL, oscsport);
#endif

    if ((surface = r2sdl_begin(w, h))) {
        SDL_WM_SetCaption("Hypercube", "Hypercube");
	f_init();
	cube4d_init();
	d_init();
	rb = r_alloc(w, h);
        tb = r_alloc(w, h);
	
        t = 0;
        while (running) {
            if (!paused) {
                r_clear(rb, 0);
                p_planar8bit(&p, t);
                d_transform(
                    &cube4d_vertex, &v4,
                    a[0][1], a[0][2], a[0][3], a[1][2], a[1][3], a[2][3]
                );
                n_project(&v4, d1, d2, &v2);
                n_scale2screen(&v2, w, h);
                n_layered(rb, tb, &v2);
                n_wireframe(rb, &v2, 0);
                r2sdl_setpalette(&p);
                r2sdl_display(rb);
                t++;
#ifdef HAVE_LIBLO
		osc_send_v2(sender, &v2);
#endif
            }
	    if (save) {
	        snprintf(filename, 60, fileformat, filecount++);
	        sdl2png(filename, surface);
	        save--;
	    }
            while (SDL_PollEvent(&event)) {
                switch (event.type) {
                case SDL_QUIT:
                    running = 0;
                    break;
                case SDL_KEYDOWN:
                    switch (event.key.keysym.sym) {
                    case SDLK_ESCAPE: running = 0; break;
                    case SDLK_SPACE: paused = !paused; break;
		    case SDLK_PRINT: save = savecount; break;
                    case SDLK_q: a[0][1]++; break;
                    case SDLK_w: a[0][2]++; break;
                    case SDLK_e: a[0][3]++; break;
                    case SDLK_r: a[1][2]++; break;
                    case SDLK_t: a[1][3]++; break;
                    case SDLK_y: a[2][3]++; break;
                    case SDLK_a: a[0][1]=0; break;
                    case SDLK_s: a[0][2]=0; break;
                    case SDLK_d: a[0][3]=0; break;
                    case SDLK_f: a[1][2]=0; break;
                    case SDLK_g: a[1][3]=0; break;
                    case SDLK_h: a[2][3]=0; break;
                    case SDLK_z: a[0][1]--; break;
                    case SDLK_x: a[0][2]--; break;
                    case SDLK_c: a[0][3]--; break;
                    case SDLK_v: a[1][2]--; break;
                    case SDLK_b: a[1][3]--; break;
                    case SDLK_n: a[2][3]--; break;
                    default: break;
                    }
                    break;
                default:
                    break;
                }
            }
#ifdef HAVE_LIBLO
	    pollfds[0].fd = oscfd;
	    pollfds[0].events = POLLIN | POLLPRI;
	    if (poll(pollfds, 1, 0) > 0) {
                lo_server_recv_noblock(oscserver, 0);
	    }
#endif
            SDL_Delay(40);
	}

        r_free(tb);
        r_free(rb);
	r2sdl_end();
    }

    return (0);
}

/* EOF */

