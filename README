hypercube -- interaction in four dimensions.
Copyright (C) 2006,2017 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

version:   0.3.13
requires:  libsdl, libpng, liblo
compile:   make
usage:     ./hypercube <width> <height> <depth1> <depth2> <savecount> <listenport> <sendport>
example:   ./hypercube 640 480 512 1024 1 7777 8888
keys:
 ESC         -- exit (or use window close button)
 SPACE       -- pause/unpause
 PRINTSCREEN -- save frame to numbered PNG file(*)
 q,w,e,r,t,y -- increase rotation speeds
 a,s,d,f,g,h -- zero rotation speeds
 z,x,c,v,b,n -- decrease rotation speeds
open sound control:
 /hypercube/quit -- quit
 /hypercube/rotate/inc  dim1:f dim2:f -- increment rotation speeds
 /hypercube/rotate/zero dim1:f dim2:f -- stop rotations
 /hypercube/rotate/dec  dim1:f dim2:f -- decrement rotation speeds
 /hypercube/vertex2d n:i x:f y:f -- raw vertex coordinates
 (see included Pd files for examples of control and sonification)

(*) each time you press PRINTSCREEN savecount images will be
saved in the current directory in PNG format, with filenames
like hypercube-00000000.png, hypercube-00000001.png, etc.
Note that the counter restarts from 0000000 each time you
restart hypercube, so you might want to move the previously
saved images out of the way to avoid overwriting them.
These frames can be converted to animated gif using:
find *.png -exec convert {} {}.gif \;
gifsicle --colors 256 --delay 4 --optimize --loopcount forever \
*.png.gif > hypercube.gif
