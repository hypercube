/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006, 2007 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef RASTER_H
#define RASTER_H 1
/* raster drawing algorithms */

#include "stack.h"

#define RASTER_BPP (1)

typedef unsigned char pixel;
typedef struct {
    int w;
    int h;
    pixel *data;
} raster;

raster *r_alloc(int w, int h);
void r_free(raster *rb);
void r_clear(raster *rb, pixel pen);
void r_plot(raster *rb, int x, int y, pixel pen);
pixel r_get(raster *rb, int x, int y);
void r_hline(raster *rb, int x0, int y, int x1, pixel pen);
void r_line(raster *rb, int x0, int y0, int x1, int y1, pixel pen);
void r_poly(raster *rb, stack *p, pixel pen);
void r_bounds(raster *rb, stack *p, fixed *bounds, int clear_bounds);
void r_fillpoly(raster *rb, stack *p, pixel pen);
void r_copy(raster *to, raster *from);
void r_bitor(raster *to, raster *from);
void r_bitor_bounded(raster *to, const raster *from, const fixed *bounds);
void r_bitand(raster *to, raster *from);
void r_bitxor(raster *to, raster *from);

/* EOF */
#endif
