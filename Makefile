# hypercube -- interaction in four dimensions.
# Copyright (C) 2006, 2007 Claude Heiland-Allen
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
# Makefile

OPTFLAGS = -O3
FEATURES = -DHAVE_LIBLO
# FEATURES =

CC = gcc
CFLAGS = $(OPTFLAGS) $(FEATURES) -Wall `sdl-config --cflags` `libpng-config --cflags` `pkg-config liblo --cflags`
LIBS = `sdl-config --libs` `libpng-config --libs` `pkg-config liblo --libs` -lm
RM = rm -f

OFILES = cube4d.o dynamics.o fixed.o geometry.o hull2d.o palette.o \
r2sdl.o raster.o render.o sdl2png.o stack.o

#

all: hypercube

clean:
	$(RM) $(OFILES)

dist-clean:
	$(RM) hypercube

#

cube4d.o: cube4d.c cube4d.h fixed.h geometry.h stack.h
	$(CC) $(CFLAGS) -o cube4d.o -c cube4d.c

dynamics.o: dynamics.c dynamics.h cube4d.h fixed.h geometry.h
	$(CC) $(CFLAGS) -o dynamics.o -c dynamics.c

fixed.o: fixed.c fixed.h
	$(CC) $(CFLAGS) -o fixed.o -c fixed.c

geometry.o: geometry.c geometry.h fixed.h
	$(CC) $(CFLAGS) -o geometry.o -c geometry.c

hull2d.o: hull2d.c hull2d.h fixed.h geometry.h stack.h
	$(CC) $(CFLAGS) -o hull2d.o -c hull2d.c

palette.o: palette.c palette.h fixed.h
	$(CC) $(CFLAGS) -o palette.o -c palette.c

r2sdl.o: r2sdl.c r2sdl.h palette.h raster.h
	$(CC) $(CFLAGS) -o r2sdl.o -c r2sdl.c

raster.o: raster.c raster.h stack.h
	$(CC) $(CFLAGS) -o raster.o -c raster.c

render.o: render.c render.h cube4d.h fixed.h geometry.h hull2d.h \
raster.h stack.h
	$(CC) $(CFLAGS) -o render.o -c render.c

sdl2png.o: sdl2png.c sdl2png.h
	$(CC) $(CFLAGS) -o sdl2png.o -c sdl2png.c

stack.o: stack.c stack.h
	$(CC) $(CFLAGS) -o stack.o -c stack.c

#

hypercube: hypercube.c cube4d.h dynamics.h fixed.h geometry.h \
palette.h r2sdl.h raster.h render.h stack.h cube4d.o dynamics.o fixed.o \
geometry.o hull2d.o palette.o r2sdl.o raster.o render.o sdl2png.o stack.o
	$(CC) $(CFLAGS) -o hypercube hypercube.c cube4d.o dynamics.o \
fixed.o geometry.o hull2d.o palette.o r2sdl.o raster.o render.o sdl2png.o \
stack.o $(LIBS)

# EOF
