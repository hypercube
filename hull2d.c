/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006,2017 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
/* 2d convex hull */

#include "stack.h"
#include "geometry.h"
#include "fixed.h"

static comparator hull2d_cmp;

static int hull2d_cmp(stack_content a, stack_content b) {
    vector2 *u = (vector2 *) a;
    vector2 *v = (vector2 *) b;
    int c = f_cmp((*u)[0], (*v)[0]);
    if (c) return (c);
    return (f_cmp((*u)[1], (*v)[1]));
}

void hull2d(stack *in, stack *to) {
    int i;
    stack p, u, l;
    s_copy(in, &p);
    s_sort(&p, hull2d_cmp);
    s_uniq(&p, hull2d_cmp);
    s_clear(&u);
    s_push(&u, s_at(&p, 0));
    s_push(&u, s_at(&p, 1));
    for (i = 2; i < s_size(&p); i++) {
        s_push(&u, s_at(&p, i));
        while (s_size(&u) > 2 &&
               !v2_rturn((vector2 *) s_at(&u, -3), (vector2 *) s_at(&u, -2), (vector2 *) s_at(&u, -1))) {
            s_delete(&u, -2);
        }
    }
    s_reverse(&p);
    s_clear(&l);
    s_push(&l, s_at(&p, 0));
    s_push(&l, s_at(&p, 1));
    for (i = 2; i < s_size(&p); i++) {
        s_push(&l, s_at(&p, i));
        while (s_size(&l) > 2 &&
               !v2_rturn((vector2 *) s_at(&l, -3), (vector2 *) s_at(&l, -2), (vector2 *) s_at(&l, -1))) {
            s_delete(&l, -2);
        }
    }
    s_delete(&l, 0);
    s_delete(&l, -1);
    s_copy(&u, to);
    s_append(&l, to);
}

/* EOF */
