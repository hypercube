/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006,2017 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#include <SDL.h>
#include <png.h>

#include "sdl2png.h"

/* WARNING: this code is not robust, it assumes indexed palette
mode with 256 entries, if this is not the case it will crash */

int sdl2png(char *filename, SDL_Surface *surface) {
    FILE *fp = NULL;
    png_struct *png_ptr = NULL;
    png_info *info_ptr = NULL;
    png_color palette[256];
    int c, pass, passes, y;

    if ((fp = fopen(filename, "wb")) == NULL) {
        return (1);
    }
    png_ptr = png_create_write_struct(
        PNG_LIBPNG_VER_STRING,
	(void *) NULL, NULL, NULL
    );
    if (png_ptr == NULL) {
	return (2);
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (info_ptr == NULL) {
	png_destroy_write_struct(&png_ptr, (png_infopp) NULL);
	return(3);
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
	png_destroy_write_struct(&png_ptr, &info_ptr);
	return (4);
    }

    png_init_io(png_ptr, fp);
    png_set_compression_level(png_ptr, 9);
    png_set_IHDR(
        png_ptr,
	info_ptr, 
	surface->w,
	surface->h,
	8, /* bit depth */
	PNG_COLOR_TYPE_PALETTE,
	PNG_INTERLACE_ADAM7,
	PNG_COMPRESSION_TYPE_DEFAULT,
	PNG_FILTER_TYPE_DEFAULT
    );
    for (c = 0; c < 256; c++) {
	palette[c].red   = surface->format->palette->colors[c].r;
	palette[c].green = surface->format->palette->colors[c].g;
	palette[c].blue  = surface->format->palette->colors[c].b;
    }
    png_set_PLTE(png_ptr, info_ptr, palette, 256);
    png_write_info(png_ptr, info_ptr);
    passes = png_set_interlace_handling(png_ptr);
    SDL_LockSurface(surface);
    for (pass = 0; pass < passes; pass++) {
	for (y = 0; y < surface->h; y++) {
	    png_write_row(
	        png_ptr,
		((png_bytep) (surface->pixels)) + surface->pitch * y
	    );
	}
    }
    SDL_UnlockSurface(surface);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);
    fclose(fp);
    return (0);
}

/* EOF */
