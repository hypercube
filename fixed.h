/*
hypercube -- interaction in four dimensions.
Copyright (C) 2006 Claude Heiland-Allen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/
#ifndef FIXED_H
#define FIXED_H 1
/* fixed point arithmetic */

typedef signed short fixed;

#define FIXED_ONE (128)
#define FIXED_BITS (7)
#define f_itof(a) ((fixed)((a)<<FIXED_BITS))

#define f_add(a,b) ((a)+(b))
#define f_sub(a,b) ((a)-(b))
#define f_neg(a)   (-(a))
#define f_mul(a,b) (((a)*(b))>>FIXED_BITS)
#define f_div(a,b) ((fixed)((((int)(a))<<FIXED_BITS)/(b)))
int f_cmp(fixed a, fixed b);

extern fixed f_sintab[512];
#define FIXED_TABSIZE (512)
#define FIXED_TABMASK (511)
#define FIXED_COSOFFSET (128)
#define f_sin(a)   (f_sintab[(a)&FIXED_TABMASK])
#define f_cos(a)   (f_sintab[((a)+FIXED_COSOFFSET)&FIXED_TABMASK])

void f_init(void);

/* EOF */
#endif
